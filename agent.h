#pragma once
#include <string>
#include <random>
#include <sstream>
#include <vector>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include <stdarg.h>
#include <fstream> 
#include <iostream>

#define TILE_MAX 33

class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			property[key] = { value };
		}
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string name() const { return property.at("name"); }
	virtual std::string role() const { return property.at("role"); }
	virtual void notify(const std::string& msg) { property[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> property;
};

/**
 * evil (environment agent)
 * add a new random tile on board, or do nothing if the board is full
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public agent {
public:
	rndenv(const std::string& args = "") : agent("name=rndenv role=environment " + args) {
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));

		if (property.find("layer") != property.end())
			search_layer = int(property["layer"]);
		else 
			search_layer = 0;
	}

	virtual action take_action(const board& after) {
		int space[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		int mini_score = -1;
		int best_pos = -1;

		for (int pos : space) {
			board b = after;
			if (after(pos) != 0) continue;
			std::uniform_int_distribution<int> popup(0, 3);
			int tile = popup(engine) ? 1 : 3;

			b(pos) = tile;
			float score = MiniMaxSearch(b, search_layer);
			if ((best_pos == -1) || (score < mini_score)) {
				mini_score = score;
				best_pos = pos;
			}

			return action::place(tile, pos);
		}
		return action();
	}

	void add_tuple(int table, int n, ...) {
		va_list ap;
		va_start(ap, n);
		int* patt = new int[n+2];
		patt[0] = table;
		patt[1] = n;
		int table_size = 0;
		int factor = TILE_MAX;
		for (int i = 2; i < n+2; i++) {
			patt[i] = va_arg(ap, int);
			table_size += factor;
			factor = factor * TILE_MAX;
		}

		if(state_value_map.find(table) == state_value_map.end()) {
			state_value_map[table] = new float[table_size];
		}

		tuple_list.push_back(patt);
	}

	int get_state_value(const board& before) {
		int value = 0;
		for(int i=0; i<tuple_list.size(); i++) {
			int* pattern = tuple_list[i];
			board b = before;
			for (int trans=0; trans<2; trans++) {
				for(int rot=0; rot<3; rot++) {
					int key = 0;

					int factor = 1.0;
					for (int j=2; j<pattern[1]+2; j++) {
						key += b( pattern[j] ) * factor;
						factor *= TILE_MAX;
					}
					
					value += (state_value_map.find(pattern[0])->second)[key];

					b.rotate_right();
				}
				b.transpose();
			}
		}

		return value;
	}

	void load_weight(std::string file_path) {
		std::ifstream fin(file_path);
		if (!fin){
			std::cout << "The model file doesn't exist: " << file_path << std::endl;
			return;
		}

		printf("The evil load model from %s\n", file_path.c_str());
		int key = -1;
		std::string k, v;
		while(!fin.eof()) {
			fin >> k >> v;
			if(k.find("map") == 0) {
				key = std::stoi(v);
				std::cout << "load tbale: " << key << std::endl;
			} else {
				float* value_table = state_value_map.find(key)->second;
				value_table[std::stoi(k)] = std::stof(v);
			}
		}

		fin.close();
	}

private:
	std::default_random_engine engine;

	int search_layer;
	std::vector< int* > tuple_list;
	std::map<int, float* > state_value_map;

	float MiniMaxSearch(const board& after, int layer) {
		if (layer == 0) {
			return get_state_value(after);
		}

		return beforeSearch(after, layer);
	}

	float afterSearch(const board& after, int times) {
		float v = NULL;
		for (int i=0; i<16; i++) {
			board b = after;
			if (b(i) == 0) {
				b(i) = 1;

				float v_temp = 0;
				if (times == 0)
					v_temp = get_state_value(b);
				else
					v_temp = beforeSearch(b, times-1);
				if (v == NULL) {
					v = v_temp;
				} else {
					v = v < v_temp ? v : v_temp;
				}

				b(i) = 3;
				if (times == 0)
					v_temp = get_state_value(b);
				else
					v_temp = beforeSearch(b, times-1);
				v = v < v_temp ? v : v_temp;
			}
		}

		return v;
	}

	float beforeSearch(const board& after, int times) {
		float v = NULL;
		for (int i=0; i<4; i++) {
			board b = after;
			int r = b.move(i);
			if (r != -1) {
				float v_temp = 0;
				v_temp = afterSearch(b, times);

				v = v > (r + v_temp) ? v : (r + v_temp);
			}
		}

		return v;
	}
};

/**
 * player (dummy)
 * select an action randomly
 */
class player : public agent {
public:
	player(const std::string& args = "") : agent("name=player " + args) {
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));
	}

	virtual action take_action(const board& before) {
		int opcode[] = { 0, 1, 2, 3 };
		std::shuffle(opcode, opcode + 4, engine);
		for (int op : opcode) {
			board b = before;
			if (b.move(op) != -1) return action::move(op);
		}
		return action();
	}

private:
	std::default_random_engine engine;
};

class HeuristicAIPlayer : public agent {
public:
	HeuristicAIPlayer(const std::string& args = "") : agent("name=HeuristicAIPlayer " + args) {
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));
	}

	virtual action take_action(const board& before) {
		int opcode[] = { 0, 1, 2, 3 };
		std::shuffle(opcode, opcode + 4, engine);
		int max = -1;
		int max_op = 0;
		for (int op : opcode) {
			board b = before;
			int score = b.move(op);
            if (score == -1) continue;

			int bonus = 0;
			int max_tile = 0;
			int max_tile_position = 0;
			for (int i=0; i<4; i++) {
				for (int j=0; j<4; j++) {
					if (b[i][j] > max_tile) {
						max_tile = b[i][j];
						max_tile_position = i*4 + j;
					}

					int dis = 0;
					if (j == 3 && i < 3) {
						dis = b[i][j] - b[i+1][j];
						if (dis == 1 || dis == -1) {
							bonus = board::index_to_value((b[i][j] > b[i+1][j]? b[i][j] : b[i+1][j])+1);
						}
					} else if (i == 3 && j < 3) {
						dis = b[i][j] - b[i][j+1];
						if (dis == 1 || dis == -1) {
							bonus = board::index_to_value((b[i][j] > b[i][j+1]? b[i][j] : b[i][j+1])+1);
						}
					} else if (i == 3 && j == 3) {
						continue;
					} else {
						dis = b[i][j] - b[i][j+1];
						if (dis == 1 || dis == -1) {
							bonus = board::index_to_value((b[i][j] > b[i][j+1]? b[i][j] : b[i][j+1])+1);
						}

						dis = b[i][j] - b[i+1][j];
						if (dis == 1 || dis == -1) {
							bonus = board::index_to_value((b[i][j] > b[i+1][j]? b[i][j] : b[i+1][j])+1);
						}
					}
				}
			}

			score += bonus + max_tile_position;
			if (score > max) {
				max = score;
				max_op = op;
			}
		}
		return action::move(max_op);
	}

private:
	std::default_random_engine engine;
};

class TDAIPlayer : public agent {

public:
	TDAIPlayer(const std::string& args = "") : agent("name=player role=player " + args) {
		if (property.find("seed") != property.end())
			engine.seed(int(property["seed"]));

		if (property.find("layer") != property.end())
			search_layer = int(property["layer"]);
		else 
			search_layer = 0;
	}

	virtual void open_episode(const std::string& flag = "") {
		episode.clear();
		episode.reserve(32768);
	}

	virtual void close_episode(const std::string& flag = "") {
	}

	virtual action take_action(const board& before) {
		int opcode[] = { 0, 1, 2, 3 };
		int max_score = -1;
		int best_op = -1;

		for (int op : opcode) {
			board b = before;
			float score = b.move(op);
			if (score != -1) {
				// score += get_state_value(b);
				// score += afterSearch(b);

				score += expectimanxSearch(b, search_layer);

				if ((best_op == -1) || (score > max_score)) {
					max_score = score;
					best_op = op;
				}
			}
		}

		if (best_op != -1)
			store_episode(before, best_op);

		return action::move(best_op);
	}

	void add_tuple(int table, int n, ...) {
		va_list ap;
		va_start(ap, n);
		int* patt = new int[n+2];
		patt[0] = table;
		patt[1] = n;
		int table_size = 0;
		int factor = TILE_MAX;
		for (int i = 2; i < n+2; i++) {
			patt[i] = va_arg(ap, int);
			table_size += factor;
			factor = factor * TILE_MAX;
		}

		if(state_value_map.find(table) == state_value_map.end()) {
			state_value_map[table] = new float[table_size];
		}

		tuple_list.push_back(patt);
	}

	int get_state_value(const board& before) {
		int value = 0;
		for(int i=0; i<tuple_list.size(); i++) {
			int* pattern = tuple_list[i];
			board b = before;
			for (int trans=0; trans<2; trans++) {
				for(int rot=0; rot<3; rot++) {
					int key = 0;

					int factor = 1.0;
					for (int j=2; j<pattern[1]+2; j++) {
						key += b( pattern[j] ) * factor;
						factor *= TILE_MAX;
					}
					
					value += (state_value_map.find(pattern[0])->second)[key];

					b.rotate_right();
				}
				b.transpose();
			}
		}

		return value;
	}

	void update_state_value(float learning_rate) {
		for(int i=episode.size() - 1; i >= 0; i--) {
			float td_error = 0.0;

			board s = episode[i].after_state;
			if(i == episode.size() - 1) {
				td_error = 0.0 - get_state_value(s);
			} else {
				board s_next = episode[i+1].after_state;
				td_error = episode[i].reward + get_state_value(s_next) - get_state_value(s);
			}

			for(int i=0; i<tuple_list.size(); i++) {
				int* pattern = tuple_list[i];
				board b = s;
				for (int trans=0; trans<2; trans++) {
					for(int rot=0; rot<3; rot++) {
						int key = 0;

						int factor = 1.0;
						for (int j=2; j<pattern[1]+2; j++) {
							key += b( pattern[j] ) * factor;
							factor *= TILE_MAX;
						}

						float* value_table = state_value_map.find(pattern[0])->second;
						value_table[key] += td_error * learning_rate/32;

						b.rotate_right();
					}
					b.transpose();
				}
			}
		}
	}

	void save_weight(std::string file_path) {

		std::ofstream fout(file_path);
		if (!fout){
			std::cout << "There are diretories which doesn't exist." << std::endl;
			return;
		}

		printf("Store model to %s\n", file_path.c_str());

		for(int i=0; i<tuple_list.size(); i++) {
			int table_number = tuple_list[i][0];
			int table_size = 0;
			for (int j = 0; j < tuple_list[i][1]; j++) {
				table_size += pow(TILE_MAX, j+1);
			}


			fout << "map" << "\t" << table_number << std::endl;
			std::cout << "Save tbale: " << table_number << std::endl;
			float* values = state_value_map.find(table_number)->second;
			for(int j=0; j<table_size; j++) {
				if (values[j])
					fout << j << "\t" << values[j] << std::endl;
			}

		}

		fout.close();
	}

	void load_weight(std::string file_path) {
		std::ifstream fin(file_path);
		if (!fin){
			std::cout << "The model file doesn't exist: " << file_path << std::endl;
			return;
		}

		printf("The player load model from %s\n", file_path.c_str());
		int key = -1;
		std::string k, v;
		while(!fin.eof()) {
			fin >> k >> v;
			if(k.find("map") == 0) {
				key = std::stoi(v);
				std::cout << "load tbale: " << key << std::endl;
			} else {
				float* value_table = state_value_map.find(key)->second;
				value_table[std::stoi(k)] = std::stof(v);
			}
		}

		fin.close();
	}


private:
	struct state {
		board after_state;
		action move;
		int reward;
	};

	std::default_random_engine engine;

	int search_layer;

	std::vector<state> episode;
	std::vector< int* > tuple_list;
	std::map<int, float* > state_value_map;

	void store_episode(const board& before, int op) {
		board b = before;
		int reward = b.move(op);
		state s = {.after_state=b, .move=action::move(op), .reward=reward};

		episode.push_back(s);
	}

	float expectimanxSearch(const board& after, int layer) {
		if (layer == 0) {
			return get_state_value(after);
		}

		return afterSearch(after, layer);
	}

	float afterSearch(const board& after, int times) {
		float v = NULL;
		for (int i=0; i<16; i++) {
			board b = after;
			if (b(i) == 0) {
				b(i) = 1;
				float v_temp = beforeSearch(b, times);
				if (v == NULL) {
					v = v_temp;
				} else {
					v = v < v_temp ? v : v_temp;
				}

				b(i) = 3;
				v = v < v_temp ? v : v_temp;
			}
		}

		return v;
	}

	float beforeSearch(const board& after, int times) {
		float v = NULL;
		for (int i=0; i<4; i++) {
			board b = after;
			int r = b.move(i);
			if (r != -1) {
				float v_temp = 0;
				if (times == 0)
					v_temp = get_state_value(b);
				else
					v_temp = afterSearch(b, times-1);

				v = v > (r + v_temp) ? v : (r + v_temp);
			}
		}

		return v;
	}
};
